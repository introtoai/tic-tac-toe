﻿using System.Net.NetworkInformation;
using TicToe;

class MyGame
{
    static void Main(string[] args)
    {
        Console.WriteLine("Welcome to the game!");
        string start = "y";
        int playerId = 0;
        //game session
        while (start.ToLower() == "y")
        {
            Console.WriteLine("Type 1 for computer to start, type 2 for you to start. Hit Enter!");
            int inputNumber;
            string input = Console.ReadLine();
            bool successParse = int.TryParse(input, out inputNumber);
            if (successParse || inputNumber!= 1 || inputNumber!= 2)
            {
                playerId = inputNumber;
            }
            Game game = new Game(playerId);
            game.DisplayBoard();
            //game is on while there are still availeble possition
            while (game.AvailablePositions.Length > 0)
            {
                //define a flag for successful move
                bool isSucessPlayer1 = false;
                bool isSucessPlayer2 = false;

                if (game.IsComputerMoveNow(game.Board) == true)
                {
                    //check if there is a winner before the move
                    if (game.IsGameOn("Player 2"))
                    {
                        while (!isSucessPlayer1)
                        {
                            isSucessPlayer1 = game.Player1Move();
                        }
                    }
                    else
                    {
                        break;
                    }
                }

                if (game.IsGameOn("Player 1"))
                {
                    while (!isSucessPlayer2)
                    {
                        isSucessPlayer2 = game.Player2Move();
                    }
                }
                else
                {
                    break;
                }
            }
            Console.WriteLine("Nicely Done!");
            Console.WriteLine("Type y to start over! Type any other character to exit!");
            Console.WriteLine();
            start = Console.ReadLine();
        }
        Console.WriteLine("Good Bye!!! ");
    }
}












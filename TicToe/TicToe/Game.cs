﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicToe
{
    public class Game
    {
        public bool IsPlayer1Move { get; set; }
        public int IsComputerStarts { get; set; }
        public char[] Board { get; set; }
        public int[] AvailablePositions { get; set; }
        public char Player1Mark { get; set; }
        public char Player2Mark { get; set;}

        //fucction to remove from available positions
        public bool RemoveFromAvailablePositions(int input)
        {
            bool isPositionAvailable = AvailablePositions.Contains(input);
            if (input < 1 || input > 9)
            {
                Console.WriteLine("Error! Outside of index!");
                return false;
            }
            //check if input is in current array
            else if (!isPositionAvailable)
            {
                Console.WriteLine("{0} is already taken", input);
                return false;
            }
            else
            {
                AvailablePositions = AvailablePositions.Where(value => value != input).ToArray();
            }
            return true;
        }

        //funtion to update the board
        public void UpdateBoard(int userInput, bool isPlayer1Move)
        {
            if (isPlayer1Move)
            {
                Board[userInput - 1] = Player1Mark;
            }
            else
            {
                Board[userInput - 1] = Player2Mark;
            }

        }

        //function to check if a winner found
        public bool CheckIfWinnerFound(char[] currentBoardState, char currMark)
        {
            if (
                (currentBoardState[0] == currMark && currentBoardState[1] == currMark && currentBoardState[2] == currMark) ||
                (currentBoardState[3] == currMark && currentBoardState[4] == currMark && currentBoardState[5] == currMark) ||
                (currentBoardState[6] == currMark && currentBoardState[7] == currMark && currentBoardState[8] == currMark) ||
                (currentBoardState[0] == currMark && currentBoardState[3] == currMark && currentBoardState[6] == currMark) ||
                (currentBoardState[1] == currMark && currentBoardState[4] == currMark && currentBoardState[7] == currMark) ||
                (currentBoardState[2] == currMark && currentBoardState[5] == currMark && currentBoardState[8] == currMark) ||
                (currentBoardState[0] == currMark && currentBoardState[4] == currMark && currentBoardState[8] == currMark) ||
                (currentBoardState[2] == currMark && currentBoardState[4] == currMark && currentBoardState[6] == currMark)
                )
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //function to check if we continue the game or exit
        public bool IsGameOn(string player)
        {
            char mark = Player1Mark;
            if (player == "Player 2")
                mark = Player2Mark;

            if (CheckIfWinnerFound(Board, mark))
            {
                Console.WriteLine("Game Over! {0} won !!!", player);
                return false;
            }

            if (AvailablePositions.Length > 0)
            {
                return true;
            }
            Console.WriteLine("Game Over! We have a draw!");
            return false;
        }

        //function to display curernt board
        public void DisplayBoard()
        {
            Console.Clear();
            Console.WriteLine("Player 1 (Computer) - X; Player 2 - O");
            Console.WriteLine("Type number 1 to 9 to mark your position on the board. Hit Enter!");
            Console.WriteLine();
            Console.WriteLine("     |     |      ");
            Console.WriteLine("  {0}  |  {1}  |  {2}", Board[0], Board[1], Board[2]);
            Console.WriteLine("_____|_____|_____ ");
            Console.WriteLine("     |     |      ");
            Console.WriteLine("  {0}  |  {1}  |  {2}", Board[3], Board[4], Board[5]);
            Console.WriteLine("_____|_____|_____ ");
            Console.WriteLine("     |     |      ");
            Console.WriteLine("  {0}  |  {1}  |  {2}", Board[6], Board[7], Board[8]);
            Console.WriteLine("     |     |      ");
            Console.WriteLine();
            Console.WriteLine("--------------------");
            Console.WriteLine();
        }
        //set of actions for player 1 to do
        public bool Player1Move()
        {
            TestPlay recommendedMove = MiniMax(Board, Player1Mark);
            IsPlayer1Move = true;
            int input = int.Parse(recommendedMove.Char.ToString());
            bool isSuccess = RemoveFromAvailablePositions(input);
            if (isSuccess)
            {
                UpdateBoard(input, IsPlayer1Move);
                DisplayBoard();
                return true;
            }
            return false;

        }
        //set of actions for player 2 to do
        public bool Player2Move()
        {
            IsPlayer1Move = false;
            Console.WriteLine("Player 2, your move...");
            int inputNumber;
            string input = Console.ReadLine();
            bool successParse = int.TryParse(input, out inputNumber);
            bool isSuccess = false;
            if (successParse)
            {
                isSuccess = RemoveFromAvailablePositions(inputNumber);
            }
            if (isSuccess)
            {
                UpdateBoard(inputNumber, IsPlayer1Move);
                DisplayBoard();
                return true;
            }
            return false;
        }

        //function to get all emptry cells/positions on the current board state
        public int[] GetAllEmptyCellsIndexes(char[] currentBoardState)
        {
            int[] indexes = new int[0];
            for (int i = 0; i < currentBoardState.Length; i++)
            {
                if (currentBoardState[i] != 'X' && currentBoardState[i] != 'O')
                {
                    indexes = indexes.Append(i).ToArray();
                }
            }
            return indexes;
        }

        //Minimax algorithm
        public TestPlay MiniMax(char[] currentBoardState, char currMark)
        {
            TestPlay testPlayInfo = new TestPlay();
            // Store the indexes of all empty cells:
            int[] availCellsIndexes = GetAllEmptyCellsIndexes(currentBoardState);

            // Check if there is a terminal state:
            if (CheckIfWinnerFound(currentBoardState, Player2Mark))
            {
                testPlayInfo.Score = -1;
                return testPlayInfo;
            }
            else if (CheckIfWinnerFound(currentBoardState, Player1Mark))
            {
                testPlayInfo.Score = 1;
                return testPlayInfo;
            }
            else if (availCellsIndexes.Length == 0)
            {
                testPlayInfo.Score = 0;
                return testPlayInfo;
            }

            // Store the outcome of each test:
            List<TestPlay> allTestPlayInfos = new List<TestPlay>();

            //for-loop statement that will loop through each of the empty cells:
            for (int i = 0; i < availCellsIndexes.Length; i++)
            {
                // store this test-play’s terminal score:
                TestPlay currentTestPlayInfo = new TestPlay();

                // Save the char of the cell this for-loop is currently processing:
                currentTestPlayInfo.Char = currentBoardState[availCellsIndexes[i]];
                currentTestPlayInfo.Index = Array.IndexOf(currentBoardState, currentBoardState[availCellsIndexes[i]]);

                // Place the current player’s mark on the cell for-loop is currently processing:
                currentBoardState[availCellsIndexes[i]] = currMark;

                if (currMark ==Player1Mark)
                {
                    // Recursively run the minimax function for the new board:
                    TestPlay result = MiniMax(currentBoardState, Player2Mark);

                    // Save the result variable’s score into the currentTestPlayInfo object:
                    currentTestPlayInfo.Score = result.Score;
                }
                else
                {
                    // Recursively run the minimax function for the new board:
                    TestPlay result = MiniMax(currentBoardState, Player1Mark);

                    // Save the result variable’s score into the currentTestPlayInfo object:
                    currentTestPlayInfo.Score = result.Score;
                }

                //Reset the current board back to the state it was before the current player made its move:
                currentBoardState[availCellsIndexes[i]] = currentTestPlayInfo.Char;

                //Save the result of the current player’s test-play for future use:
                allTestPlayInfos.Add(currentTestPlayInfo);
            }

            //Create a store for the best test-play’s reference:
            TestPlay bestTestPlay = new TestPlay();

            //Get the reference to the current player’s best test-play:
            if (currMark == Player1Mark)
            {
                int bestScore = int.MinValue;
                for (int i = 0; i < allTestPlayInfos.Count; i++)
                {
                    if (allTestPlayInfos[i].Score > bestScore)
                    {
                        bestScore = allTestPlayInfos[i].Score;
                        bestTestPlay.Index = allTestPlayInfos[i].Index;
                        bestTestPlay.Char = allTestPlayInfos[i].Char;

                    }
                }
            }
            else
            {
                int bestScore = int.MaxValue;
                for (int i = 0; i < allTestPlayInfos.Count; i++)
                {
                    if (allTestPlayInfos[i].Score < bestScore)
                    {
                        bestScore = allTestPlayInfos[i].Score;
                        bestTestPlay.Index = allTestPlayInfos[i].Index;
                        bestTestPlay.Char = allTestPlayInfos[i].Char;
                        bestTestPlay.Score = bestScore;
                    }
                }
            }

            //Get the object with the best test-play score for the current player:
            TestPlay results = allTestPlayInfos.FirstOrDefault(el => el.Index == bestTestPlay.Index);
            return results;
        }

        //function to check whose turn is now
        public bool IsComputerMoveNow(char[] boardState)
        {
            int countX = 0;
            int countO = 0;
            //count how many X and O we have on the board
            for (int i = 0; i < boardState.Length; i++)
            {
                if (boardState[i] == 'X')
                    countX++;
                if (boardState[i] == 'O')
                    countO++;
            }
            if (countX == countO && countX == 0 && IsComputerStarts == 0)
            {
                return false;
            }
            if (countX > countO)
            {
                return false;
            }

            return true;
        }
        
        //constructor to init the game
        public Game(int computerStart)
        {
            if (computerStart == 1)
            {
                IsComputerStarts = 1;
            }
            else
            {
                IsComputerStarts = 0;
            }
            IsPlayer1Move = true;
            Board = new char[] { '1', '2', '3', '4', '5', '6', '7', '8', '9' };
            AvailablePositions = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            Player1Mark = 'X';
            Player2Mark = 'O';
        }
    }
}

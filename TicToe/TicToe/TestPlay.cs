﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicToe
{
    public class TestPlay
    {
        public char Char { get; set; }
        public int Score { get; set; }
        public int Index { get; set; }
    }
}
